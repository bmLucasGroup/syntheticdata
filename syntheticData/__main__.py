import os
import random
import string

TARGET_COUNT = 2000000  # Total number of files and folders including the skeleton
FILE_SIZE_RANGE = (1 * 1024, 120 * 1024)  # 42 KB to 100 MB
MAX_FOLDERS_IN_FOLDER = 6  # Maximum number of folders in each folder
MAX_NEST_LEVEL = 12  # Maximum nesting level relative to the root

PROJECT_NAME = "ProjectName"
SUBDIRS = [
    "01_Deliveries",
    "02_Preproduction",
    "03_Resources",
    "04_Editorial",
    "05_Assets",
    "06_Shots",
    "07_Wip",
    "08_Reviews"
]


def create_file(path, size):
    """Create a binary file with random content of a specified size."""
    with open(path, 'wb') as f:
        f.write(os.urandom(size))


def create_structure(root):
    """Create the initial directory structure."""
    for subdir in SUBDIRS:
        os.makedirs(os.path.join(root, subdir), exist_ok=True)


def count_items(root):
    """Count all files and folders under the root directory."""
    total_items = 0
    for _, dirs, files in os.walk(root):
        total_items += len(dirs) + len(files)
    return total_items


def get_random_subdir_path(root):
    """Generate a random subdirectory path up to MAX_NEST_LEVEL deep."""
    depth = random.randint(1, MAX_NEST_LEVEL)
    subdir = random.choice(SUBDIRS)
    path = os.path.join(root, subdir)
    for _ in range(depth):
        path = os.path.join(path, ''.join(random.choices(string.ascii_letters + string.digits, k=10)))
    return path


def populate_structure(root, target_count, file_size_range, max_folders_in_folder):
    """Populate the structure with files and folders until the target count is reached."""
    total_count = count_items(root)

    while total_count < target_count:
        current_path = get_random_subdir_path(root)
        os.makedirs(current_path, exist_ok=True)

        # Count new intermediate directories
        new_dirs = current_path.count(os.sep) - root.count(os.sep) - 1
        total_count += new_dirs

        # Randomly decide the number of items (files and folders) to create in the current path
        num_items = random.randint(1, max_folders_in_folder)

        for _ in range(num_items):
            if total_count >= target_count:
                break

            if random.choice([True, False]):
                # Create file
                file_size = random.randint(file_size_range[0], file_size_range[1])
                file_path = os.path.join(current_path,
                                         ''.join(random.choices(string.ascii_letters + string.digits, k=10)))
                if not os.path.exists(file_path):
                    create_file(file_path, file_size)
                    total_count += 1
            else:
                # Create folder
                folder_path = os.path.join(current_path,
                                           ''.join(random.choices(string.ascii_letters + string.digits, k=10)))
                if not os.path.exists(folder_path):
                    os.makedirs(folder_path, exist_ok=True)
                    total_count += 1  # Count the new folder
                    # Ensure at least one file in the new folder
                    file_size = random.randint(file_size_range[0], file_size_range[1])
                    file_path = os.path.join(folder_path,
                                             ''.join(random.choices(string.ascii_letters + string.digits, k=10)))
                    create_file(file_path, file_size)
                    total_count += 1  # Count the new file


if __name__ == "__main__":
    root = PROJECT_NAME
    os.makedirs(root, exist_ok=True)
    create_structure(root)
    populate_structure(root, TARGET_COUNT, FILE_SIZE_RANGE, MAX_FOLDERS_IN_FOLDER)
    print(f"Dataset created with {TARGET_COUNT} files and folders in '{root}'")
