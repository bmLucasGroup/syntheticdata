# syntheticData

This repo helps generating a dataset to stress test different file systems.
It has control with the number of files, size and nested directories.

### Parameters

- TARGET_COUNT: Total number of files and folders including the skeleton
- FILE_SIZE_RANGE: Range in bytes of the min and max file size. 
- MAX_FOLDERS_IN_FOLDER: Maximum number of folders in each folder
- MAX_NEST_LEVEL: Maximum nesting level relative to the root


## Install

```
git clone <repo>
cd syntheticdata
python -m syntheticData
```

## How to use

```
cd syntheticdata
python -m syntheticData
```
